﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Data.Interface;
using ProjectManagerApi.Model;

namespace ProjectManagerApi.Business
{
    public class ParentTaskService : IService<ParentTask>
    {
        private readonly IRepository<Model.ParentTask> parentTaskRepository;

        public ParentTaskService(IRepository<Model.ParentTask> parentTaskRepository)
        {
            this.parentTaskRepository = parentTaskRepository;
        }

        public Task<int> Create(ParentTask entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ParentTask> Get(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ParentTask>> GetAll()
        {
            return await this.parentTaskRepository.GetAll();
        }

        public Task<int> Update(ParentTask entity)
        {
            throw new NotImplementedException();
        }
    }
}
