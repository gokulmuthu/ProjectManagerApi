﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Business.Mapper;
using ProjectManagerApi.Data.Interface;
using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi.Business
{
    public class UserService : IService<UserDetail>
    {
        private readonly IRepository<Model.User> userRepository;

        public UserService(IRepository<Model.User> userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<int> Create(UserDetail entity)
        {
            return await this.userRepository.Create(UserMapper.Map(entity));
        }

        public async Task<int> Delete(int id)
        {
            return await this.userRepository.Delete(id);
        }

        public async Task<UserDetail> Get(int id)
        {
            return UserMapper.Map(await this.userRepository.Get(id));
        }

        public async Task<IEnumerable<UserDetail>> GetAll()
        {
            return UserMapper.Map(await this.userRepository.GetAll());
        }

        public async Task<int> Update(UserDetail entity)
        {
            return await this.userRepository.Update(UserMapper.Map(entity));
        }
    }
}
