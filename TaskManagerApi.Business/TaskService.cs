﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Business.Mapper;
using ProjectManagerApi.Data.Interface;
using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi.Business
{
    public class TaskService : IService<TaskDetail>
    {
        private readonly IRepository<Model.Task> taskRepository;

        public TaskService(IRepository<Model.Task> taskRepository)
        {
            this.taskRepository = taskRepository;
        }

        public async Task<int> Create(TaskDetail entity)
        {
            return await this.taskRepository.Create(TaskMapper.Map(entity));
        }

        public async Task<int> Delete(int id)
        {
            return await this.taskRepository.Delete(id);
        }

        public async Task<TaskDetail> Get(int id)
        {
            return TaskMapper.Map(await this.taskRepository.Get(id));
        }

        public async Task<IEnumerable<TaskDetail>> GetAll()
        {
            return TaskMapper.Map(await this.taskRepository.GetAll());
        }

        public async Task<int> Update(TaskDetail entity)
        {
            return await this.taskRepository.Update(TaskMapper.Map(entity));
        }
    }
}
