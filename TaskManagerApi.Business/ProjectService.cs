﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Business.Mapper;
using ProjectManagerApi.Data.Interface;
using ProjectManagerApi.Model;

namespace ProjectManagerApi.Business
{
    public class ProjectService : IService<Model.Contracts.ProjectDetail>
    {
        private readonly IRepository<Project> projectRepository;

        public ProjectService(IRepository<Project> projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        public async Task<int> Create(Model.Contracts.ProjectDetail entity)
        {
            return await this.projectRepository.Create(ProjectMapper.Map(entity));
        }

        public async Task<int> Delete(int id)
        {
            return await this.projectRepository.Delete(id);
        }

        public async Task<Model.Contracts.ProjectDetail> Get(int id)
        {
            return ProjectMapper.Map(await this.projectRepository.Get(id));
        }

        public async Task<IEnumerable<Model.Contracts.ProjectDetail>> GetAll()
        {
            return ProjectMapper.Map(await this.projectRepository.GetAll());
        }

        public async Task<int> Update(Model.Contracts.ProjectDetail entity)
        {
            return await this.projectRepository.Update(ProjectMapper.Map(entity));
        }
    }
}
