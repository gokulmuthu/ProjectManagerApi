﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagerApi.Business.Mapper
{
    public static class ProjectMapper
    {
        public static Model.Contracts.ProjectDetail Map(Model.Project source)
        {
            if (source == null)
            {
                return null;
            }

            return MapProject(source);
        }

        public static Model.Project Map(Model.Contracts.ProjectDetail source)
        {
            if (source == null)
            {
                return null;
            }

            return MapProject(source);
        }

        public static IEnumerable<Model.Contracts.ProjectDetail> Map(IEnumerable<Model.Project> sourceList)
        {
            if (sourceList == null)
            {
                return null;
            }

            var targetList = new List<Model.Contracts.ProjectDetail>();
            sourceList.ToList().ForEach(source => targetList.Add(MapProject(source)));


            return targetList;
        }

        private static Model.Contracts.ProjectDetail MapProject(Model.Project source)
        {
            return new Model.Contracts.ProjectDetail
            {
                Id = source.Id,
                Name = source.Name,
                Priority = source.Priority,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                IsComplete = source.IsComplete,
                NumberOfTasks = source.Tasks != null ? source.Tasks.Count : 0,
                UserDetail = UserMapper.Map(source.User)
            };
        }

        private static Model.Project MapProject(Model.Contracts.ProjectDetail source)
        {
            return new Model.Project
            {
                Id = source.Id,
                Name = source.Name,
                Priority = source.Priority,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                IsComplete = source.IsComplete,
                User = UserMapper.Map(source.UserDetail)
            };
        }
    }
}
