﻿
using ProjectManagerApi.Model.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagerApi.Business.Mapper
{
    public static class UserMapper
    {
        public static UserDetail Map(Model.User source)
        {
            if (source == null)
            {
                return null;
            }

            return MapUserDetail(source);
        }

        public static Model.User Map(UserDetail source)
        {
            if (source == null)
            {
                return null;
            }

            return new Model.User
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                EmployeeId = source.EmployeeId
            };
        }
        public static IEnumerable<UserDetail> Map(IEnumerable<Model.User> sourceList)
        {
            if (sourceList == null)
            {
                return null;
            }

            var targetList = new List<Model.Contracts.UserDetail>();
            sourceList.ToList().ForEach(source => targetList.Add(MapUserDetail(source)));

            return targetList;
        }

        public static Model.User MapProject(Model.User entity, int projectId)
        {
            if (entity.Project != null)
            {
                entity.Project.Id = projectId;
            }
            else
            {
                entity.Project = new Model.Project
                {
                    Id = projectId
                };
            }

            return entity;
        }

        private static UserDetail MapUserDetail(Model.User source)
        {
            return new UserDetail
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                EmployeeId = source.EmployeeId
            };
        }
    }
}
