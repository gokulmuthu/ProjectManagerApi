﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectManagerApi.Business.Mapper
{
    public class TaskMapper
    {
        public static Model.Contracts.TaskDetail Map(Model.Task source)
        {
            if (source == null)
            {
                return null;
            }

            return MapTaskDetail(source);
        }

        public static Model.Task Map(Model.Contracts.TaskDetail source)
        {
            if (source == null)
            {
                return null;
            }

            return MapTask(source);
        }

        public static IEnumerable<Model.Contracts.TaskDetail> Map(IEnumerable<Model.Task> sourceList)
        {
            if (sourceList == null)
            {
                return null;
            }

            var targetList = new List<Model.Contracts.TaskDetail>();
            sourceList.ToList().ForEach(source => targetList.Add(MapTaskDetail(source)));


            return targetList;
        }

        private static Model.Contracts.TaskDetail MapTaskDetail(Model.Task source)
        {
            return new Model.Contracts.TaskDetail
            {
                Id = source.Id,
                Name = source.Name,
                Priority = source.Priority,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                IsComplete = source.IsComplete,
                IsParent = source.IsParent,
                ParentTask = MapToParentTask(source.ParentTask),
                Project = ProjectMapper.Map(source.Project),
                User = UserMapper.Map(source.User)
            };
        }

        private static Model.Task MapTask(Model.Contracts.TaskDetail source)
        {
            return new Model.Task
            {
                Id = source.Id,
                Name = source.Name,
                Priority = source.Priority,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                IsParent = source.IsParent,
                IsComplete = source.IsComplete,
                ParentTask = MapToTask(source.ParentTask),
                Project = ProjectMapper.Map(source.Project),
                User = UserMapper.Map(source.User)
            };
        }

        private static Model.Task MapToTask(Model.ParentTask parentTask)
        {
            if (parentTask == null)
            {
                return null;
            }

            return new Model.Task
            {
                Id = parentTask.Id,
                Name = parentTask.Name
            };
        }

        private static Model.ParentTask MapToParentTask(Model.Task parentTask)
        {
            if (parentTask == null)
            {
                return null;
            }

            return new Model.ParentTask
            {
                Id = parentTask.Id,
                Name = parentTask.Name
            };
        }
    }
}
