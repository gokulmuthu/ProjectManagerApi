﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Test.Infrastructure;
using Xunit;

namespace ProjectManagerApi.Data.Test
{
    public class ProjectRepositoryTest : ProjectManagerDbTestBase
    {
        #region GetAll

        [Fact]
        public async Task GetAll_Returns_CorrectType()
        {
            // Arrange
            var repository = new ProjectRepository(dbContext);

            // Act
            var results = await repository.GetAll();

            // Assert
            Assert.IsAssignableFrom<IEnumerable<Model.Project>>(results);
        }

        [Fact]
        public async Task GetAll_Returns_AllProjects()
        {
            // Arrange
            var repository = new ProjectRepository(dbContext);

            // Act
            var results = await repository.GetAll();

            // Assert
            Assert.Equal(8, results.Count());
        }
        
        #endregion

        #region Get

        [Fact]
        public async Task Get_Returns_CorrectType()
        {
            // Arrange
            var repository = new ProjectRepository(dbContext);

            // Act
            var result = await repository.Get(1);

            // Assert
            Assert.IsAssignableFrom<Model.Project>(result);
        }

        [Fact]
        public async Task Get_Returns_Expected_Project()
        {
            // Arrange
            var repository = new ProjectRepository(dbContext);

            // Act
            var result = await repository.Get(2);

            // Assert
            Assert.Equal(2, result.Id);
            Assert.Equal("Project 2", result.Name);
        }
        
        #endregion

        #region Create

        [Fact]
        public async Task Create_Inserts_NewProject()
        {
            // Arrange
            var repository = new ProjectRepository(dbContext);

            // Act
            var result = await repository.Create(new Model.Project { Id= 9, Name = "Project 9", Priority = 1, StartDate = new DateTime(), EndDate = new DateTime().AddDays(1), User = new Model.User { Id = 1} });
            var actualProjects = await repository.GetAll();

            // Assert
            Assert.Equal(9, actualProjects.Count());
        }

        #endregion

        #region Update

        [Fact]
        public async Task Update_Saves_Project_With_WithChanges()
        {
            // Arrange
            var repository = new ProjectRepository(dbContext);

            var project = await repository.Get(1);
            project.Name = "New Project Name";
            project.Priority = 2;
            project.User = new Model.User { Id = 1 };

            // Act
            var result = await repository.Update(project);
            var actualTask = await repository.Get(1);

            // Assert
            Assert.Equal(project.Id, actualTask.Id);
            Assert.Equal(project.Name, actualTask.Name);
            Assert.Equal(project.Priority, actualTask.Priority);
        }

        #endregion
    }
}
