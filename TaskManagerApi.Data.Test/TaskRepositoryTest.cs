﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Test.Infrastructure;
using Xunit;

namespace ProjectManagerApi.Data.Test
{
    public class TaskRepositoryTest : ProjectManagerDbTestBase
    {
        #region GetAll

        [Fact]
        public async Task GetAll_Returns_CorrectType()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);

            // Act
            var results = await repository.GetAll();

            // Assert
            Assert.IsAssignableFrom<IEnumerable<Model.Task>>(results);
        }

        [Fact]
        public async Task GetAll_Returns_AllTasks()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);

            // Act
            var results = await repository.GetAll();

            // Assert
            Assert.Equal(6, results.Count());
        }

        #endregion

        #region Get

        [Fact]
        public async Task Get_Returns_CorrectType()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);

            // Act
            var result = await repository.Get(1);

            // Assert
            Assert.IsAssignableFrom<Model.Task>(result);
        }

        [Fact]
        public async Task Get_Returns_ExpectedTask()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);

            // Act
            var result = await repository.Get(2);

            // Assert
            Assert.Equal(2, result.Id);
            Assert.Equal("Task 2", result.Name);
        }

        [Fact]
        public async Task Get_Include_ParentTask()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);

            // Act
            var result = await repository.Get(3);

            // Assert
            Assert.NotNull(result.ParentTask);
            Assert.Equal(1, result.ParentTask.Id);
            Assert.Equal("Task 1", result.ParentTask.Name);
        }

        #endregion

        #region Create

        [Fact]
        public async Task Create_Inserts_NewTask_With_NoParentTask()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);
            var parentTaskRepository = new ParentTaskRepository(dbContext);

            // Act
            var result = await repository.Create(new Model.Task { Id= 10, Name = "Task 10", Priority = 1, IsParent = true, Project = new Model.Project { Id = 1 }, User = new Model.User { Id = 1 } });
            var actualTasks = await repository.GetAll();
            var actualParentTasks = await parentTaskRepository.GetAll();

            // Assert
            Assert.Equal(7, actualTasks.Count());
        }

        #endregion

        #region Update

        [Fact]
        public async Task Update_Saves_Task_With_WithChanges()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);
            var parentTaskRepository = new ParentTaskRepository(dbContext);

            var task = await repository.Get(1);
            task.Name = "New Task Name";
            task.Priority = 2;

            // Act
            var result = await repository.Update(task);
            var actualTask = await repository.Get(1);
            var actualParentTasks = await parentTaskRepository.GetAll();

            // Assert
            Assert.Equal(task.Id, actualTask.Id);
            Assert.Equal(task.Name, actualTask.Name);
            Assert.Equal(task.Priority, actualTask.Priority);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Delete_Update_IsCompleted_To_True()
        {
            // Arrange
            var repository = new TaskRepository(dbContext);

            // Act
            var result = await repository.Delete(1);
            var actualTask = await repository.Get(1);

            // Assert
            Assert.True(actualTask.IsComplete);
        }

        #endregion
    }
}
