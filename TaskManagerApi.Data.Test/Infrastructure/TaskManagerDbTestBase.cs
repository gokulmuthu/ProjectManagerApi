﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManagerApi.Data.Test.Infrastructure
{
    public class ProjectManagerDbTestBase : IDisposable
    {
        protected readonly ProjectManagerDbContext dbContext;

        public ProjectManagerDbTestBase()
        {
            var options = new DbContextOptionsBuilder<ProjectManagerDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            dbContext = new ProjectManagerDbContext(options);

            dbContext.Database.EnsureCreated();

            ProjectManagerDbInitializer.Initialize(dbContext);
        }

        public void Dispose()
        {
            dbContext.Database.EnsureDeleted();

            dbContext.Dispose();
        }
    }
}
