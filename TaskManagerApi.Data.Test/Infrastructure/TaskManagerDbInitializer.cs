﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectManagerApi.Model;

namespace ProjectManagerApi.Data.Test.Infrastructure
{
    public class ProjectManagerDbInitializer
    {
        public static void Initialize(ProjectManagerDbContext dbContext)
        {
            if (dbContext.Tasks.Any())
            {
                return;
            }

            Seed(dbContext);
        }

        private static void Seed(ProjectManagerDbContext dbContext)
        {
            var tasks = GetTasks();
            var users = GetUsers();
            var projects = GetProjects();

            dbContext.Users.AddRange(users);
            dbContext.Projects.AddRange(projects);
            dbContext.Tasks.AddRange(tasks);
            dbContext.SaveChanges();
        }

        private static IList<Task> GetTasks()
        {
            return new List<Task>
            {
                new Task { Id = 1, Name = "Task 1", Priority = 1, IsParent = true, Project = new Project { Id = 4 }, User = new User { Id = 3 } },
                new Task { Id = 2, Name = "Task 2", Priority = 1, IsParent = true, Project = new Project { Id = 5 }, User = new User { Id = 4 } },
                new Task { Id = 3, Name = "Task 3", Priority = 1, StartDate = new DateTime(), ParentTask = new Task { Id = 1, Name = "Task 1" }, Project = new Project { Id = 5 }, User = new User { Id = 5 } },
                new Task { Id = 4, Name = "Task 4", Priority = 1, StartDate = new DateTime(), ParentTask = new Task { Id = 2, Name = "Task 2" }, Project = new Project { Id = 6 }, User = new User { Id = 6 } },
                new Task { Id = 5, Name = "Task 5", Priority = 1, StartDate = new DateTime(), ParentTask = new Task { Id = 1, Name = "Task 1" }, Project = new Project { Id = 7 }, User = new User { Id = 7 } },
                new Task { Id = 6, Name = "Task 6", Priority = 1, StartDate = new DateTime(), Project = new Project { Id = 8 }, User = new User { Id = 8 } }
            };
        }

        private static IList<Project> GetProjects()
        {
            return new List<Project>
            {
                new Project { Id = 1, Name = "Project 1", StartDate = new DateTime(), EndDate = new DateTime().AddDays(1) },
                new Project { Id = 2, Name = "Project 2", StartDate = new DateTime(), EndDate = new DateTime().AddDays(1), IsComplete = true },
                new Project { Id = 3, Name = "Project 3", StartDate = new DateTime(), EndDate = new DateTime().AddDays(1) }
            };
        }

        private static IList<User> GetUsers()
        {
            return new List<User>
            {
                new User { Id = 1, FirstName = "First Name 1", LastName = "Last Name 1", EmployeeId = 1 },
                new User { Id = 2, FirstName = "First Name 2", LastName = "Last Name 2", EmployeeId = 2 }
            };
        }
    }
}
