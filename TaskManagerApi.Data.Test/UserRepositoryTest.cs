﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Test.Infrastructure;
using Xunit;

namespace ProjectManagerApi.Data.Test
{
    public class UserRepositoryTest : ProjectManagerDbTestBase
    {
        #region GetAll

        [Fact]
        public async Task GetAll_Returns_CorrectType()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            // Act
            var results = await repository.GetAll();

            // Assert
            Assert.IsAssignableFrom<IEnumerable<Model.User>>(results);
        }

        [Fact]
        public async Task GetAll_Returns_AllUsers()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            // Act
            var results = await repository.GetAll();

            // Assert
            Assert.Equal(8, results.Count());
        }
        
        #endregion

        #region Get

        [Fact]
        public async Task Get_Returns_CorrectType()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            // Act
            var result = await repository.Get(1);

            // Assert
            Assert.IsAssignableFrom<Model.User>(result);
        }

        [Fact]
        public async Task Get_Returns_Expected_User()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            // Act
            var result = await repository.Get(2);

            // Assert
            Assert.Equal(2, result.Id);
            Assert.Equal("First Name 2", result.FirstName);
        }
        
        #endregion

        #region Create

        [Fact]
        public async Task Create_Inserts_NewUser()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            // Act
            var result = await repository.Create(new Model.User { Id = 9, FirstName = "User First Name", LastName = "User Last Name", EmployeeId = 20 });
            var actualProjectss = await repository.GetAll();

            // Assert
            Assert.Equal(9, actualProjectss.Count());
        }

        #endregion

        #region Update

        [Fact]
        public async Task Update_Saves_User_With_WithChanges()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            var user = await repository.Get(1);
            user.FirstName = "Updated First Name";
            user.LastName = "Updated Last Name";
            user.EmployeeId = 30;

            // Act
            var result = await repository.Update(user);
            var actualUser = await repository.Get(1);

            // Assert
            Assert.Equal(user.Id, actualUser.Id);
            Assert.Equal(user.FirstName, actualUser.FirstName);
            Assert.Equal(user.LastName, actualUser.LastName);
            Assert.Equal(user.EmployeeId, actualUser.EmployeeId);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Delete_Removes_User()
        {
            // Arrange
            var repository = new UserRepository(dbContext);

            // Act
            var result = await repository.Delete(1);
            var actualUser = await repository.Get(1);

            // Assert
            Assert.Null(actualUser);
        }

        #endregion
    }
}
