﻿using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi.Model.Mapper
{
    public static class ParentTaskMapper
    {
        public static ParentTask Map(CreateParentTask newParentTask)
        {
            return new ParentTask
            {
                Id = newParentTask.Id,
                Name = newParentTask.Name
            };
        }

        public static ParentTask Map(UpdateParentTask updateParentTask)
        {
            if (updateParentTask == null)
            {
                return null;
            }

            return new ParentTask
            {
                Id = updateParentTask.Id,
                Name = updateParentTask.Name,
            };
        }
    }
}
