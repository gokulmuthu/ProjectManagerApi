﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi.Model.Mapper
{
    public static class TaskMapper
    {
        public static TaskDetail Map(CreateTask newTask)
        {
            var task = new TaskDetail
            {
                Name = newTask.Name,
                Priority = newTask.Priority,
                StartDate = newTask.StartDate,
                EndDate = newTask.EndDate,
                IsParent = newTask.IsParent,
                Project = new ProjectDetail
                {
                    Id = newTask.ProjectId
                },
                User = new UserDetail
                {
                    Id = newTask.UserId
                }
            };

            if (newTask.ParentTask != null)
            {
                task.ParentTask = ParentTaskMapper.Map(newTask.ParentTask);
            }

            return task;
        }

        public static TaskDetail Map(UpdateTask updateTask)
        {
            return new TaskDetail
            {
                Id = updateTask.Id,
                Name = updateTask.Name,
                Priority = updateTask.Priority,
                StartDate = updateTask.StartDate,
                EndDate = updateTask.EndDate,
                ParentTask = ParentTaskMapper.Map(updateTask.ParentTask),
                IsParent = updateTask.IsParent,
                User = new UserDetail
                {
                    Id = updateTask.UserId
                },
                Project = new ProjectDetail
                {
                    Id = updateTask.ProjectId
                }
            };
        }
    }
}
