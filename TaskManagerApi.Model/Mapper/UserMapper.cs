﻿using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi.Model.Mapper
{
    public static class UserMapper
    {
        public static UserDetail Map(CreateUser newUser)
        {
            var user = new UserDetail
            {
                FirstName = newUser.FirstName,
                LastName = newUser.LastName,
                EmployeeId = newUser.EmployeeId
            };

            return user;
        }

        public static UserDetail Map(UpdateUser updateUser)
        {
            var user = new UserDetail
            {
                Id = updateUser.Id,
                FirstName = updateUser.FirstName,
                LastName = updateUser.LastName,
                EmployeeId = updateUser.EmployeeId
            };

            return user;
        }
    }
}
