﻿using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi.Model.Mapper
{
    public static class ProjectMapper
    {
        public static ProjectDetail Map(CreateProject newProject)
        {
            var project = new ProjectDetail
            {
                Name = newProject.Name,
                Priority = newProject.Priority,
                StartDate = newProject.StartDate,
                EndDate = newProject.EndDate,
                UserDetail = new UserDetail
                {
                    Id = newProject.UserId
                }
            };

            return project;
        }

        public static ProjectDetail Map(UpdateProject updateTask)
        {
            var entity = new ProjectDetail
            {
                Id = updateTask.Id,
                Name = updateTask.Name,
                Priority = updateTask.Priority,
                StartDate = updateTask.StartDate,
                EndDate = updateTask.EndDate,
                UserDetail = new UserDetail
                {
                    Id = updateTask.UserId
                }
            };
            
            return entity;
        }
    }
}
