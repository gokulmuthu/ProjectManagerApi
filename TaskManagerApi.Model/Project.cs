﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectManagerApi.Model
{
    public class Project
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Priority { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool IsComplete { get; set; }

        public IList<Task> Tasks { get; set; }

        public User User { get; set; }
    }
}
