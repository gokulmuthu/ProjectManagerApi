﻿using System;

namespace ProjectManagerApi.Model.Contracts
{
    public class TaskDetail : Task
    {
        public int Id { get; set; }
        
        public ParentTask ParentTask { get; set; }
        
        public bool IsComplete { get; set; }

        public ProjectDetail Project { get; set; }

        public UserDetail User { get; set; }
    }
}
