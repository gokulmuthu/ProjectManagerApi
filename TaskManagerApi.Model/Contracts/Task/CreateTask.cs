﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagerApi.Model.Contracts
{
    public class CreateTask : Task
    {
        public CreateParentTask ParentTask { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}
