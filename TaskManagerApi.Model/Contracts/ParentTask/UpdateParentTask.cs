﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagerApi.Model.Contracts
{
    public class UpdateParentTask
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
