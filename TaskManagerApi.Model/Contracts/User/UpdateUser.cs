﻿using System.ComponentModel.DataAnnotations;

namespace ProjectManagerApi.Model.Contracts
{
    public class UpdateUser : CreateUser
    {
        [Required]
        public int Id { get; set; }
    }
}
