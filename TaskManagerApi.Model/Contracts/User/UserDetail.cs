﻿
namespace ProjectManagerApi.Model.Contracts
{
    public class UserDetail
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int EmployeeId { get; set; }

    }
}
