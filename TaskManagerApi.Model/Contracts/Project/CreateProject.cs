﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagerApi.Model.Contracts
{
    public class CreateProject
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Priority { get; set; }

         public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}
