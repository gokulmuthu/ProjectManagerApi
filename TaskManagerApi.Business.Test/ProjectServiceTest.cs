
using Moq;
using System;
using System.Threading.Tasks;
using ProjectManagerApi.Data;
using ProjectManagerApi.Data.Interface;
using Xunit;

namespace ProjectManagerApi.Business.Test
{
    public class ProjectServiceTest
    {
        #region Private Fields

        private Mock<IRepository<Model.Project>> mockRepository;
        private ProjectService projectService;
        
        #endregion

        #region Constructor

        public ProjectServiceTest()
        {
            mockRepository = new Mock<IRepository<Model.Project>>();
            projectService = new ProjectService(mockRepository.Object);
        }

        #endregion

        #region GetAll

        [Fact]
        public async Task GetAll_Calls_ProjectRepository_GetAll_Once()
        {
            // Arrange && Act
            var result = await projectService.GetAll();

            // Assert
            mockRepository.Verify(r => r.GetAll(), Times.Once);
        }

        #endregion

        #region Get

        [Fact]
        public async Task Get_Calls_ProjectRepository_Get_Once()
        {
            // Arrange && Act
            var result = await projectService.Get(1);

            // Assert
            mockRepository.Verify(r => r.Get(It.IsAny<int>()), Times.Once);
        }

        #endregion

        #region Create

        [Fact]
        public async Task Create_Calls_ProjectRepository_Create_Once()
        {
            // Arrange && Act
            var result = await projectService.Create(new Model.Contracts.ProjectDetail());

            // Assert
            mockRepository.Verify(r => r.Create(It.IsAny<Model.Project>()), Times.Once);
        }
        
        #endregion

        #region Update

        [Fact]
        public async Task Update_Calls_ProjectRepository_Update_Once()
        {
            // Arrange && Act
            var result = await projectService.Update(new Model.Contracts.ProjectDetail());

            // Assert
            mockRepository.Verify(r => r.Update(It.IsAny<Model.Project>()), Times.Once);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Delete_Calls_ProjectRepository_Delete_Once()
        {
            // Arrange && Act
            var result = await projectService.Delete(1);

            // Assert
            mockRepository.Verify(r => r.Delete(It.IsAny<int>()), Times.Once);
        }

        #endregion
    }
}