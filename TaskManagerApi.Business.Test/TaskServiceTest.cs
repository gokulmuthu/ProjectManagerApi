
using Moq;
using System;
using System.Threading.Tasks;
using ProjectManagerApi.Data;
using ProjectManagerApi.Data.Interface;
using Xunit;

namespace ProjectManagerApi.Business.Test
{
    public class TaskServiceTest
    {
        #region Private Fields

        private Mock<IRepository<Model.Task>> mockRepository;
        private TaskService taskService;
        
        #endregion

        #region Constructor

        public TaskServiceTest()
        {
            mockRepository = new Mock<IRepository<Model.Task>>();
            taskService = new TaskService(mockRepository.Object);
        }

        #endregion

        #region GetAll

        [Fact]
        public async Task GetAll_Calls_TaskRepository_GetAll_Once()
        {
            // Arrange && Act
            var result = await taskService.GetAll();

            // Assert
            mockRepository.Verify(r => r.GetAll(), Times.Once);
        }

        #endregion

        #region Get

        [Fact]
        public async Task Get_Calls_TaskRepository_Get_Once()
        {
            // Arrange && Act
            var result = await taskService.Get(1);

            // Assert
            mockRepository.Verify(r => r.Get(It.IsAny<int>()), Times.Once);
        }

        #endregion

        #region Create

        [Fact]
        public async Task Create_Calls_TaskRepository_Create_Once()
        {
            // Arrange && Act
            var result = await taskService.Create(new Model.Contracts.TaskDetail());

            // Assert
            mockRepository.Verify(r => r.Create(It.IsAny<Model.Task>()), Times.Once);
        }

        #endregion

        #region Update

        [Fact]
        public async Task Update_Calls_TaskRepository_Update_Once()
        {
            // Arrange && Act
            var result = await taskService.Update(new Model.Contracts.TaskDetail());

            // Assert
            mockRepository.Verify(r => r.Update(It.IsAny<Model.Task>()), Times.Once);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Delete_Calls_ParentTaskRepository_Delete_Once()
        {
            // Arrange && Act
            var result = await taskService.Delete(1);

            // Assert
            mockRepository.Verify(r => r.Delete(It.IsAny<int>()), Times.Once);
        }

        #endregion
    }
}