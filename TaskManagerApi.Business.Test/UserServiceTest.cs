
using Moq;
using System;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Interface;
using Xunit;

namespace ProjectManagerApi.Business.Test
{
    public class UserServiceTest
    {
        #region Private Fields

        private Mock<IRepository<Model.User>> mockRepository;
        private UserService userService;
        
        #endregion

        #region Constructor

        public UserServiceTest()
        {
            mockRepository = new Mock<IRepository<Model.User>>();
            userService = new UserService(mockRepository.Object);
        }

        #endregion

        #region GetAll

        [Fact]
        public async Task GetAll_Calls_UserRepository_GetAll_Once()
        {
            // Arrange && Act
            var result = await userService.GetAll();

            // Assert
            mockRepository.Verify(r => r.GetAll(), Times.Once);
        }

        #endregion

        #region Get

        [Fact]
        public async Task Get_Calls_UserRepository_Get_Once()
        {
            // Arrange && Act
            var result = await userService.Get(1);

            // Assert
            mockRepository.Verify(r => r.Get(It.IsAny<int>()), Times.Once);
        }

        #endregion

        #region Create

        [Fact]
        public async Task Create_Calls_UserRepository_Create_Once()
        {
            // Arrange && Act
            var result = await userService.Create(new Model.Contracts.UserDetail());

            // Assert
            mockRepository.Verify(r => r.Create(It.IsAny<Model.User>()), Times.Once);
        }
        
        #endregion

        #region Update

        [Fact]
        public async Task Update_Calls_UserRepository_Update_Once()
        {
            // Arrange && Act
            var result = await userService.Update(new Model.Contracts.UserDetail());

            // Assert
            mockRepository.Verify(r => r.Update(It.IsAny<Model.User>()), Times.Once);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Delete_Calls_UserRepository_Delete_Once()
        {
            // Act
            await userService.Delete(1);

            // Assert
            mockRepository.Verify(r => r.Delete(It.IsAny<int>()), Times.Once);
        }

        #endregion
    }
}