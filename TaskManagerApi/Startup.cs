﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using ProjectManagerApi.Business;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Data;
using ProjectManagerApi.Data.Interface;
using ProjectManagerApi.Model.Contracts;

namespace ProjectManagerApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ProjectManagerDbContext>(opts => opts.UseSqlServer(Configuration.GetConnectionString("ProjectManagerDatabase")));

            services.AddScoped<IRepository<Model.Task>, TaskRepository>();
            services.AddScoped<IRepository<Model.ParentTask>, ParentTaskRepository>();
            services.AddScoped<IRepository<Model.Project>, ProjectRepository>();
            services.AddScoped<IRepository<Model.User>, UserRepository>();

            services.AddScoped<IService<TaskDetail>, TaskService>();
            services.AddScoped<IService<Model.ParentTask>, ParentTaskService>();
            services.AddScoped<IService<ProjectDetail>, ProjectService>();
            services.AddScoped<IService<UserDetail>, UserService>();

            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info { Title = "ProjectManager", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = System.IO.Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) => {
                    swaggerDoc.Host = "dev.projectmanager.com:503";
                    swaggerDoc.Schemes = new List<string>() { httpReq.Scheme };
                });
            });
            app.UseSwaggerUI(c => {
                c.DisplayOperationId();
                c.SwaggerEndpoint("./swagger/v1/swagger.json", "ProjectManager Api");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseCors(c =>
            {
                c.AllowAnyOrigin();
                c.AllowAnyHeader();
                c.AllowAnyMethod();
                c.AllowCredentials();
            });
            app.UseMvc();
        }
    }
}
