﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Model.Contracts;
using ProjectManagerApi.Model.Mapper;

namespace ProjectManagerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDetail> projectService;
        private readonly ILogger<ProjectsController> logger;

        public ProjectsController(
            IService<ProjectDetail> projectService,
            ILogger<ProjectsController> logger)
        {
            this.projectService = projectService;
            this.logger = logger;
        }

        [HttpGet(Name = "project_query")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ProjectDetail>), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var projects = await this.projectService.GetAll();
                return Ok(projects.ToList());
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id}", Name = "project_get")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProjectDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = await this.projectService.Get(id);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost(Name = "project_post")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ProjectDetail), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] CreateProject createProject)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var newProjectId = await this.projectService.Create(ProjectMapper.Map(createProject));

                return Ok(newProjectId);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}", Name = "project_put")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ProjectDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateProject updateProject)
        {
            if (id != updateProject.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var entity = await this.projectService.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }

                await this.projectService.Update(ProjectMapper.Map(updateProject));
                
                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}/suspend", Name = "project_suspend")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProjectDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Suspend([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var entity = await this.projectService.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }

                await this.projectService.Delete(id);

                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
