﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Model.Contracts;
using ProjectManagerApi.Model.Mapper;

namespace ProjectManagerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDetail> userService;
        private readonly ILogger<UsersController> logger;

        public UsersController(IService<UserDetail> userService, ILogger<UsersController> logger)
        {
            this.userService = userService;
            this.logger = logger;
        }

        [HttpGet(Name = "user_query")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<UserDetail>), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var projects = await this.userService.GetAll();
                return Ok(projects.ToList());
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id}", Name = "user_get")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = await this.userService.Get(id);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost(Name = "user_post")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(UserDetail), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] CreateUser createUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var newUserId = await this.userService.Create(UserMapper.Map(createUser));

                return Ok(newUserId);
            }
            catch(Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}", Name = "user_put")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(UserDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateUser updateUser)
        {
            if (id != updateUser.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var entity = await this.userService.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }

                await this.userService.Update(UserMapper.Map(updateUser));

                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id}", Name = "user_delete")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(UserDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var entity = await this.userService.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }

                await this.userService.Delete(id);

                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
