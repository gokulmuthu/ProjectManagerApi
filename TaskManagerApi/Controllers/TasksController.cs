﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Model.Contracts;
using ProjectManagerApi.Model.Mapper;

namespace ProjectManagerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TaskDetail> taskService;
        private readonly IService<UserDetail> userService;
        private readonly IService<ProjectDetail> projectService;

        private readonly ILogger<TasksController> logger;

        public TasksController(
            IService<TaskDetail> taskService,
            IService<UserDetail> userService,
            IService<ProjectDetail> projectService,
            ILogger<TasksController> logger)
        {
            this.taskService = taskService;
            this.userService = userService;
            this.projectService = projectService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves all tasks.
        /// </summary>
        /// <returns>List of tasks</returns>
        [HttpGet(Name = "task_query")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<TaskDetail>), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var tasks = await this.taskService.GetAll();
                return Ok(tasks.ToList());
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Gets the task for the specified task id.
        /// </summary>
        /// <returns>A task.</returns>
        [HttpGet("{id}", Name = "task_get")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(TaskDetail), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var task = await this.taskService.Get(id);

                if (task == null)
                {
                    return NotFound();
                }

                return Ok(task);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost(Name = "task_post")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] CreateTask createTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = await this.userService.Get(createTask.UserId);

                if (user == null)
                {
                    return NotFound("User does not exists");
                }

                var project = await this.projectService.Get(createTask.ProjectId);

                if (project == null)
                {
                    return NotFound("Project does not exists");
                }

                var newTaskId = await this.taskService.Create(TaskMapper.Map(createTask));

                return Ok(newTaskId);
            }
            catch(Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Updates the task.
        /// </summary>
        /// <param name="id">The task id to update</param>
        /// <param name="updateTask">Task to update</param>
        [HttpPut("{id}", Name = "task_put")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateTask updateTask)
        {
            if (id != updateTask.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var entity = await this.taskService.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }

                await this.taskService.Update(TaskMapper.Map(updateTask));

                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Updates the task for the specified fields.
        /// </summary>
        /// <param name="id"></param>
        [HttpPut("{id}/end", Name = "task_end")]
        [Produces("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> EndTask([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var entity = await this.taskService.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }

                await this.taskService.Delete(id);

                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
