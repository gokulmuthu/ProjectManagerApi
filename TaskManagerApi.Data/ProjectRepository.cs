﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Interface;

namespace ProjectManagerApi.Data
{
    public class ProjectRepository : IRepository<Model.Project>
    {
        private readonly ProjectManagerDbContext dbContext;

        public ProjectRepository(ProjectManagerDbContext context)
        {
            this.dbContext = context;
        }

        public async Task<IEnumerable<Model.Project>> GetAll()
        {
            return await this.dbContext.Projects.Include(p => p.Tasks).Include(p => p.User).ToListAsync();
        }

        public async Task<Model.Project> Get(int id)
        {
            return await this.dbContext.Projects.Include(p => p.Tasks).Include(p => p.User).SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<int> Create(Model.Project entity)
        {
            var user = await this.dbContext.Users.SingleOrDefaultAsync(e => e.Id == entity.User.Id);
            entity.User = user;

            this.dbContext.Projects.Add(entity);
            await this.dbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<int> Update(Model.Project project)
        {
            var entity = await Get(project.Id);
            entity.Name = project.Name;
            entity.Priority = project.Priority;
            entity.StartDate = project.StartDate;
            entity.EndDate = project.EndDate;

            var user = await this.dbContext.Users.SingleOrDefaultAsync(e => e.Id == project.User.Id);
            entity.User = user;

            this.dbContext.Projects.Update(entity);
            return await this.dbContext.SaveChangesAsync();
        }

        public async Task<int> Delete(int id)
        {
            var project = await this.Get(id);
            project.IsComplete = true;

            return await this.dbContext.SaveChangesAsync();
        }
    }
}
