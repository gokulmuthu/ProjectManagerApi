﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Interface;

namespace ProjectManagerApi.Data
{
    public class TaskRepository : IRepository<Model.Task>
    {
        private readonly ProjectManagerDbContext dbContext;

        public TaskRepository(ProjectManagerDbContext context)
        {
            this.dbContext = context;
        }

        public async Task<IEnumerable<Model.Task>> GetAll()
        {
            return await this.dbContext.Tasks.Include(p => p.ParentTask).Include(p => p.Project).Include(p => p.User).ToListAsync();
        }

        public async Task<Model.Task> Get(int id)
        {
            return await this.dbContext.Tasks.Include(p => p.ParentTask).Include(p => p.Project).ThenInclude(p => p.User).Include(p => p.User).SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<int> Create(Model.Task entity)
        {
            if (entity.ParentTask != null)
            {
                entity.ParentTask = await this.dbContext.Tasks.SingleOrDefaultAsync(p => p.Id == entity.ParentTask.Id);
            }

            entity.Project = await this.dbContext.Projects.SingleOrDefaultAsync(p => p.Id == entity.Project.Id);
            entity.User = await this.dbContext.Users.SingleOrDefaultAsync(p => p.Id == entity.User.Id);

            this.dbContext.Tasks.Add(entity);
            await this.dbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<int> Update(Model.Task task)
        {
            var entity = await Get(task.Id);
            entity.Name = task.Name;
            entity.Priority = task.Priority;
            entity.StartDate = task.StartDate;
            entity.EndDate = task.EndDate;
            entity.ParentTask = task.ParentTask;
            entity.IsParent = task.IsParent;

            if (task.ParentTask != null)
            {
                entity.ParentTask = await this.dbContext.Tasks.SingleOrDefaultAsync(p => p.Id == task.ParentTask.Id);
            }

            entity.Project = await this.dbContext.Projects.SingleOrDefaultAsync(p => p.Id == task.Project.Id);
            entity.User = await this.dbContext.Users.SingleOrDefaultAsync(p => p.Id == task.User.Id);

            this.dbContext.Update(entity);
            return await this.dbContext.SaveChangesAsync();
        }

        public async Task<int> Delete(int id)
        {
            var task = await this.Get(id);
            task.EndDate = DateTime.Now;
            task.IsComplete = true;

            return await this.dbContext.SaveChangesAsync();
        }
    }
}
