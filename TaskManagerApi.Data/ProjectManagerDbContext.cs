﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectManagerApi.Model;

namespace ProjectManagerApi.Data
{
    public class ProjectManagerDbContext : DbContext
    {
        public ProjectManagerDbContext(DbContextOptions<ProjectManagerDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<ParentTask> ParentTasks { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
