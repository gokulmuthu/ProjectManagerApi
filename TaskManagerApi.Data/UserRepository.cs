﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagerApi.Data.Interface;

namespace ProjectManagerApi.Data
{
    public class UserRepository : IRepository<Model.User>
    {
        private readonly ProjectManagerDbContext dbContext;

        public UserRepository(ProjectManagerDbContext context)
        {
            this.dbContext = context;
        }

        public async Task<IEnumerable<Model.User>> GetAll()
        {
            return await this.dbContext.Users.ToListAsync();
        }

        public async Task<Model.User> Get(int id)
        {
            return await this.dbContext.Users.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<int> Create(Model.User entity)
        {
            this.dbContext.Users.Add(entity);
            await this.dbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<int> Update(Model.User user)
        {
            var entity = await Get(user.Id);
            entity.FirstName = user.FirstName;
            entity.LastName = user.LastName;
            entity.EmployeeId = user.EmployeeId;
            
            this.dbContext.Update(entity);
            return await this.dbContext.SaveChangesAsync();
        }

        public async Task<int> Delete(int id)
        {
            var entity = await Get(id);

            this.dbContext.Remove(entity);
            return await this.dbContext.SaveChangesAsync();
        }
    }
}
