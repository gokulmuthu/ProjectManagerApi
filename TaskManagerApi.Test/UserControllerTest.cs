
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Controllers;
using ProjectManagerApi.Test.Fixtures;
using Xunit;

namespace ProjectManagerApi.Test
{
    public class UserControllerTest : IClassFixture<LoggerFixture<UsersController>>
    {
        #region Private Fields

        private readonly LoggerFixture<UsersController> fixture;
        private Mock<IService<Model.Contracts.UserDetail>> mockService;
        private UsersController controller;

        #endregion

        #region Constructor

        public UserControllerTest(LoggerFixture<UsersController> loggerFixture)
        {
            fixture = loggerFixture;

            mockService = new Mock<IService<Model.Contracts.UserDetail>>();
            controller = new UsersController(mockService.Object, fixture.Logger);
        }

        #endregion

        #region GetAll

        [Fact]
        public async Task GetAll_Returns_AllUsers()
        {
            // Arrange
            mockService.Setup(service => service.GetAll()).Returns(Task.FromResult<IEnumerable<Model.Contracts.UserDetail>>(UserFixture.Users()));

            // Act
            var results = await controller.GetAll();

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(results);
            var actuals = Assert.IsAssignableFrom<IEnumerable<Model.Contracts.UserDetail>>(objectResult.Value);
            Assert.Equal(3, actuals.Count());
        }

        [Fact]
        public async Task GetAll_Throws_InternalServerError()
        {
            // Arrange
            mockService.Setup(service => service.GetAll()).Throws(new Exception());

            // Act
            var result = await controller.GetAll();

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Get

        [Fact]
        public async Task Get_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Id", "Required");

            // Act
            var result = await controller.Get(1);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Get_Returns_Expected_Users()
        {
            // Arrange
            var user = UserFixture.Users().FirstOrDefault(x => x.Id == 1);
            mockService.Setup(service => service.Get(1)).Returns(Task.FromResult<Model.Contracts.UserDetail>(user));

            // Act
            var result = await controller.Get(1);

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var actual = Assert.IsAssignableFrom<Model.Contracts.UserDetail>(objectResult.Value);
            Assert.Equal(user.Id, actual.Id);
        }

        [Fact]
        public async Task Get_Returns_NotFound_GivenInvalidId()
        {
            // Arrange
            mockService.Setup(service => service.Get(10)).Returns(Task.FromResult<Model.Contracts.UserDetail>(null));

            // Act
            var result = await controller.Get(10);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task Get_Throws_InternalServerError()
        {
            // Arrange
            mockService.Setup(service => service.Get(10)).Throws(new Exception());

            // Act
            var result = await controller.Get(10);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Create

        [Fact]
        public async Task Create_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Name", "Required");

            // Act
            var result = await controller.Create(new Model.Contracts.CreateUser());

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Create_Returns_SuccessResponse()
        {
            // Arrange
            var user = new Model.Contracts.CreateUser { FirstName = "New User", LastName = "Last Name", EmployeeId = 10 };
            mockService.Setup(service => service.Create(It.IsAny<Model.Contracts.UserDetail>())).Returns(Task.FromResult<int>(1));

            // Act
            var result = await controller.Create(user);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Create_Throws_InternalServerError()
        {
            // Arrange
            mockService.Setup(service => service.Create(It.IsAny<Model.Contracts.UserDetail>())).Throws(new Exception());

            // Act
            var result = await controller.Create(new Model.Contracts.CreateUser());

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Put

        [Fact]
        public async Task Update_Returns_BadRequest_WhenIdIsInvalid()
        {
            // Arrange
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.UserDetail>(null));

            // Act
            var result = await controller.Update(100, new Model.Contracts.UpdateUser { Id = 1});

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Update_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Name", "Required");

            // Act
            var result = await controller.Update(1, new Model.Contracts.UpdateUser { Id = 1});

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Update_Returns_NotFound_WhenIdIsInvalid()
        {
            // Arrange
            var userToUpdate = UserFixture.Users().First();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.UserDetail>(null));

            // Act
            var result = await controller.Update(userToUpdate.Id, new Model.Contracts.UpdateUser { Id = userToUpdate.Id });

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }


        [Fact]
        public async Task Update_Returns_NoContent_When_ProjectUpdated()
        {
            // Arrange
            var userToUpdate = UserFixture.Users().First();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.UserDetail>(userToUpdate));

            // Act
            var result = await controller.Update(userToUpdate.Id, new Model.Contracts.UpdateUser { Id = userToUpdate.Id });

            // Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task Update_Throws_InternalServerError()
        {
            // Arrange
            var userToUpdate = UserFixture.Users().First();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Throws(new Exception());

            // Act
            var result = await controller.Update(userToUpdate.Id, new Model.Contracts.UpdateUser { Id = userToUpdate.Id });

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Delete_Returns_NotFound_WhenIdIsInvalid()
        {
            // Arrange
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.UserDetail>(null));

            // Act
            var result = await controller.Delete(10);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }


        [Fact]
        public async Task Delete_Returns_NoContent_When_UserDeleted()
        {
            // Arrange
            var userToDelete = UserFixture.Users().Last();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.UserDetail>(userToDelete));

            // Act
            var result = await controller.Delete(userToDelete.Id);

            // Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task Delete_Throws_InternalServerError()
        {
            // Arrange
            var userToDelete = UserFixture.Users().First();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Throws(new Exception());

            // Act
            var result = await controller.Delete(userToDelete.Id);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }
        
        #endregion

    }
}