
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using ProjectManagerApi.Business.Interface;
using ProjectManagerApi.Controllers;
using ProjectManagerApi.Test.Fixtures;
using Xunit;

namespace ProjectManagerApi.Test
{
    public class ProjectControllerTest : IClassFixture<LoggerFixture<ProjectsController>>
    {
        #region Private Fields

        private readonly LoggerFixture<ProjectsController> fixture;
        private Mock<IService<Model.Contracts.ProjectDetail>> mockProjectService;
        private ProjectsController controller;

        #endregion

        #region Constructor

        public ProjectControllerTest(LoggerFixture<ProjectsController> loggerFixture)
        {
            fixture = loggerFixture;

            mockProjectService = new Mock<IService<Model.Contracts.ProjectDetail>>();
            controller = new ProjectsController(mockProjectService.Object, fixture.Logger);
        }

        #endregion

        #region GetAll

        [Fact]
        public async Task GetAll_Returns_AllProjects()
        {
            // Arrange
            mockProjectService.Setup(service => service.GetAll()).Returns(Task.FromResult<IEnumerable<Model.Contracts.ProjectDetail>>(ProjectFixture.Projects()));

            // Act
            var results = await controller.GetAll();

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(results);
            var actuals = Assert.IsAssignableFrom<IEnumerable<Model.Contracts.ProjectDetail>>(objectResult.Value);
            Assert.Equal(3, actuals.Count());
        }

        [Fact]
        public async Task GetAll_Throws_InternalServerError()
        {
            // Arrange
            mockProjectService.Setup(service => service.GetAll()).Throws(new Exception());

            // Act
            var result = await controller.GetAll();

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Get

        [Fact]
        public async Task Get_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Id", "Required");

            // Act
            var result = await controller.Get(1);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Get_Returns_Expected_Projects()
        {
            // Arrange
            var project = ProjectFixture.Projects().FirstOrDefault(x => x.Id == 1);
            mockProjectService.Setup(service => service.Get(1)).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(project));

            // Act
            var result = await controller.Get(1);

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var actual = Assert.IsAssignableFrom<Model.Contracts.ProjectDetail>(objectResult.Value);
            Assert.Equal(project.Id, actual.Id);
        }

        [Fact]
        public async Task Get_Returns_NotFound_GivenInvalidId()
        {
            // Arrange
            mockProjectService.Setup(service => service.Get(10)).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(null));

            // Act
            var result = await controller.Get(10);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task Get_Throws_InternalServerError()
        {
            // Arrange
            mockProjectService.Setup(service => service.Get(10)).Throws(new Exception());

            // Act
            var result = await controller.Get(10);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Create

        [Fact]
        public async Task Create_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Name", "Required");

            // Act
            var result = await controller.Create(new Model.Contracts.CreateProject());

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Create_Returns_SuccessResponse()
        {
            // Arrange

            var createProject = new Model.Contracts.CreateProject { Name = "New Project", Priority = 1, StartDate = new DateTime(), EndDate = new DateTime(), UserId = 1 };

            mockProjectService.Setup(service => service.Create(It.IsAny<Model.Contracts.ProjectDetail>())).Returns(Task.FromResult<int>(1));

            // Act
            var result = await controller.Create(new Model.Contracts.CreateProject());

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Create_Throws_InternalServerError()
        {
            // Arrange
            mockProjectService.Setup(service => service.Create(It.IsAny<Model.Contracts.ProjectDetail>())).Throws(new Exception());

            // Act
            var result = await controller.Create(new Model.Contracts.CreateProject());

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Put

        [Fact]
        public async Task Update_Returns_BadRequest_WhenIdIsInvalid()
        {
            // Arrange
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(null));

            // Act
            var result = await controller.Update(100, new Model.Contracts.UpdateProject { Id = 1});

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Update_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Name", "Required");

            // Act
            var result = await controller.Update(1, new Model.Contracts.UpdateProject { Id = 1});

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
        [Fact]
        public async Task Update_Returns_NotFound_WhenIdIsInvalid()
        {
            // Arrange
            var projectToUpdate = ProjectFixture.Projects().First();
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(null));

            // Act
            var result = await controller.Update(projectToUpdate.Id, new Model.Contracts.UpdateProject { Id = projectToUpdate.Id });

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }


        [Fact]
        public async Task Update_Returns_NoContent_When_ProjectUpdated()
        {
            // Arrange
            var projectToUpdate = ProjectFixture.Projects().First();
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(projectToUpdate));

            // Act
            var result = await controller.Update(projectToUpdate.Id, new Model.Contracts.UpdateProject { Id = projectToUpdate.Id });

            // Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task Update_Throws_InternalServerError()
        {
            // Arrange
            var projectToUpdate = ProjectFixture.Projects().First();
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Throws(new Exception());

            // Act
            var result = await controller.Update(projectToUpdate.Id, new Model.Contracts.UpdateProject { Id = projectToUpdate.Id });

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task Suspend_Returns_BadRequest_When_ModelStateIsInvalid()
        {
            // Arrange
            controller.ModelState.AddModelError("Name", "Required");

            // Act
            var result = await controller.Suspend(1);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
        [Fact]
        public async Task Suspend_Returns_NotFound_WhenIdIsInvalid()
        {
            // Arrange
            var projectToUpdate = ProjectFixture.Projects().First();
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(null));

            // Act
            var result = await controller.Suspend(100);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }


        [Fact]
        public async Task Delete_Returns_NoContent_When_ProjectUpdated()
        {
            // Arrange
            var projectToUpdate = ProjectFixture.Projects().First();
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Returns(Task.FromResult<Model.Contracts.ProjectDetail>(projectToUpdate));

            // Act
            var result = await controller.Suspend(1);

            // Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task Suspend_Throws_InternalServerError()
        {
            // Arrange
            var projectToUpdate = ProjectFixture.Projects().First();
            mockProjectService.Setup(service => service.Get(It.IsAny<int>())).Throws(new Exception());

            // Act
            var result = await controller.Suspend(2);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, (result as ObjectResult).StatusCode);
        }

        #endregion
    }
}