﻿using ProjectManagerApi.Model.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManagerApi.Test.Fixtures
{
    public static class UserFixture
    {
        public static IList<UserDetail> Users()
        {
            return new List<UserDetail>
            {
                new UserDetail { Id = 1, FirstName = "First Name 1", LastName = "Last Name 1", EmployeeId = 1 },
                new UserDetail { Id = 2, FirstName = "First Name 2", LastName = "Last Name 2", EmployeeId = 2 },
                new UserDetail { Id = 3, FirstName = "First Name 3", LastName = "Last Name 3", EmployeeId = 3 }
            };
        }
    }
}
