﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManagerApi.Test.Fixtures
{
    public static class ProjectFixture
    {
        public static IList<Model.Contracts.ProjectDetail> Projects()
        {
            return new List<Model.Contracts.ProjectDetail>
            {
                new Model.Contracts.ProjectDetail { Id = 1, Name = "Project 1", StartDate = new DateTime(), EndDate = new DateTime().AddDays(1) },
                new Model.Contracts.ProjectDetail { Id = 2, Name = "Project 2", StartDate = new DateTime(), EndDate = new DateTime().AddDays(1), IsComplete = true },
                new Model.Contracts.ProjectDetail { Id = 3, Name = "Project 3", StartDate = new DateTime(), EndDate = new DateTime().AddDays(1) }
            };
        }
    }
}
