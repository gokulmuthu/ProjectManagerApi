﻿using ProjectManagerApi.Model.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManagerApi.Test.Fixtures
{
    public static class TaskFixture
    {
        public static IList<TaskDetail> Tasks()
        {
            return new List<TaskDetail>
            {
                new TaskDetail { Id = 1, Name = "Task 1", Priority = 1, StartDate = new DateTime(), ParentTask = new Model.ParentTask { Id = 1, Name = "Parent Task 1" }, Project = new ProjectDetail { Id = 1 }, User = new UserDetail { Id = 1 } },
                new TaskDetail { Id = 2, Name = "Task 2", Priority = 1, StartDate = new DateTime(), ParentTask = new Model.ParentTask { Id = 2, Name = "Parent Task 2" }, Project = new ProjectDetail { Id = 1 }, User = new UserDetail { Id = 1 }  },
                new TaskDetail { Id = 3, Name = "Task 3", Priority = 1, StartDate = new DateTime(), ParentTask = new Model.ParentTask { Id = 1, Name = "Parent Task 1" }, Project = new ProjectDetail { Id = 1 }, User = new UserDetail { Id = 1 }  },
                new TaskDetail { Id = 4, Name = "Task 4", Priority = 1, StartDate = new DateTime(), Project = new ProjectDetail { Id = 1 }, User = new UserDetail { Id = 1 }  }
            };
        }
    }
}
